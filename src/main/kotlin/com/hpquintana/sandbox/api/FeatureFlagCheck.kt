package com.hpquintana.sandbox.api

import io.getunleash.Unleash
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class FeatureFlagCheck(val unleash: Unleash) {

    @GetMapping("feature-flag-check")
    fun featureFlagCheck():String {
        val toggles = unleash.more().featureToggleNames
        var toggleListString = "List of toggles available: "

        for (i in toggles) {
            if(unleash.isEnabled(i)) {
                toggleListString += "$i is enabled"
            } else {
                toggleListString += "$i is not enabled"
            }
        }

        return toggleListString
    }
}