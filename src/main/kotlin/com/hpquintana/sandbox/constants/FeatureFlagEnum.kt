package com.hpquintana.sandbox.constants

enum class FeatureFlagEnum(val flagName: String) {
    TEST_FLAG("test-ff")
}