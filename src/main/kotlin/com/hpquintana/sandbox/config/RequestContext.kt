package com.hpquintana.sandbox.config

import org.springframework.stereotype.Component
import org.springframework.web.context.annotation.RequestScope

@Component
@RequestScope
class RequestContext() {
    var userId: String = "tbd"
}