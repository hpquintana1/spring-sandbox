package com.hpquintana.sandbox.config

import io.getunleash.DefaultUnleash
import io.getunleash.Unleash
import io.getunleash.UnleashContextProvider
import io.getunleash.util.UnleashConfig
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class UnleashConfiguration {

    @Bean
    fun unleashConfig(
        @Value("\${unleashed.api.instance-id}") instanceId: String,
        @Value("\${unleashed.api.url}") apiUrl: String,
        unleashContextProvider: UnleashContextProvider
    ): UnleashConfig {
        return UnleashConfig.builder()
            .appName("spring-sandbox")
            .instanceId(instanceId)
            .unleashAPI(apiUrl)
            .unleashContextProvider(unleashContextProvider)
//            .customHttpHeader("Authorization", "clientSecret")
            .build()
    }

    @Bean
    fun unleash(unleashConfig: UnleashConfig): Unleash {
        return DefaultUnleash(unleashConfig)
    }
}