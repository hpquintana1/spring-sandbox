package com.hpquintana.sandbox.config

import io.getunleash.UnleashContext

import io.getunleash.UnleashContextProvider
import org.springframework.stereotype.Component
import org.springframework.web.context.annotation.RequestScope


@Component
@RequestScope
class CustomUnleashContextProvider(private val requestContext: RequestContext) :
    UnleashContextProvider {
    override fun getContext(): UnleashContext {
        return UnleashContext.builder()
            .userId(requestContext.userId)
            .build()
    }
}