package com.hpquintana.sandbox.config

import org.springframework.stereotype.Component
import org.springframework.web.servlet.HandlerInterceptor
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


@Component
class RequestContextInterceptor(private val requestContext: RequestContext) :
    HandlerInterceptor {
    override fun preHandle(
        request: HttpServletRequest,
        response: HttpServletResponse,
        handler: Any
    ): Boolean {
        requestContext.userId =  if(request.getHeader("user-id") != null) {
            request.getHeader("user-id")
        } else {
            "tbd"
        }
        return true
    }
}